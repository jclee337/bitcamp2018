# Speech-Activated Forth Arithmetic Calculator
This small project uses Google's speech recognition API to create a stack-based arithmetic calculator. 

Pushing and popping integers and functions on the stack works the same as in the Forth programming language.

Running eval on a stack with the contents `5 3 +` will add 3 and 5 and place the result on the top of the stack.

- Say numbers and arithmetic operations to push them to the word queue.
- Say "reset" to clear the word queue.
- Say "push" to push the word queue onto the stack (once you're happy with the result).
- Say "eval" to evaluate from the top of the stack.
